import Vue from 'vue'
import Vuex from 'vuex'
import RiskTest from './risk-test';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    RiskTest
  }
})
