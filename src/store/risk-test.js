const state = {
    labels: [
        "years",
        "family",
        "bloodPressure",
        "ethnicity",
        "wellness",
        "gender",
        "bmi"
    ],
    questions: {
        "years":{
           "label":"Tuổi", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/7-1-768x510.png",
           "title":"Bạn bao nhiêu tuổi?", 
           "content":"Tuổi càng cao thì nguy cơ tiểu đường tuýp 2 của bạn càng cao."
        }, 
        "family":{
           "label":"Tiền sử gia đình", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/9-1-768x760.png",
           "title":"Bạn có ai trong gia đình như: bố, mẹ, anh, chị, em của bạn bị tiểu đường không?", 
           "content":"Tiền sử gia đình với tiểu đường góp phần làm tăng nguy cơ mắc bệnh tiểu đường tuýp 2 của bạn."  
        },
        "bloodPressure":{
           "label":"Huyết áp", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/10-1-768x506.png",
           "title":"Bạn đã bao giờ được chẩn đoán tăng huyết áp chưa??", 
           "content":"Tăng huyết áp cũng góp phần làm tăng nguy cơ tiểu đường tuýp 2 của bạn." 
        },
        "ethnicity":{
           "label":"Dân tộc", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/12-1-768x451.png",
           "title":"Chủng tộc hay dân tộc nào mô tả đúng nhất về bạn?", 
           "content":"Những người thuộc các nhóm chủng tộc và dân tộc nhất định có nhiều khả năng mắc bệnh tiểu đường loại 2 hơn những người khác." 
        },
        "wellness":{ 
           "label":"Sức khỏe", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/11-1-768x411.png",
           "title":"Bạn có tích cực hoạt động thể chất không?",  
           "content":"Không/Ít vận động có thể làm tăng nguy cơ mắc bệnh tiểu đường tuýp 2." 
        },
        "gender":{
           "label":"Giới tính", 
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/8-1-768x456.png",
           "title":"Bạn là đàn ông hay phụ nữ?", 
           "content":"Đàn ông có nhiều khả năng hơn phụ nữ bị tiền tiểu đường mà không được chẩn đoán; một lý do có thể là họ ít đến gặp bác sĩ thường xuyên."
        }, 
        "womanSub":{
           "label":null, 
           "image":false, 
           "title":"Bạn đã bao giờ được chẩn đoán mắc bệnh tiểu đường thai kỳ?", 
           "content":"Bệnh tiểu đường thai kỳ là một loại bệnh tiểu đường phát triển ở phụ nữ trong thời kỳ mang thai. Nó thường biến mất sau khi mang thai, nhưng những phụ nữ bị tiểu đường thai kỳ có nguy cơ mắc bệnh tiểu đường loại 2."
        }, 
        "bmi":{
           "label":"BMI",
           "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/13-1-768x882.png",
           "title":"Nói thêm cho chúng tôi nghe về bạn.", 
           "content":"Sự kết hợp giữa cân nặng và chiều cao của bạn cho chúng tôi biết Chỉ số khối cơ thể hoặc BMI của bạn. Những người có BMI cao hơn có nguy cơ cao hơn.", 
           "weight":"Lựa chọn cân nặng", 
           "height":"Lựa chọn chiều cao" 
        }
    },
    variants: {
        "years":[
           {
              "id":"1.1",
              "value":"Less than 40 years",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/7-1-768x510.png",
              "label":"Younger than 40",
              "content":"<p>Khi bạn già đi, nguy cơ mắc bệnh tiểu đường loại 2 tăng lên. Những người từ 50 tuổi trở lên thì tự động đạt mức điểm nguy cơ cao hơn do vấn đề tuổi tác. Bạn không thể dừng quá trình lão hóa, nhưng bạn có thể thực hiện các bước để giảm thiểu rủi ro thông qua thay đổi lối sống và \/ hoặc chế độ y tế.<\/p>"
           },
           {
              "id":"1.2",
              "value":"40-49 Years",
              "score":1,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/7-1-768x510.png",
              "label":"40-49 Years",
              "content":"<p>Khi bạn già đi, nguy cơ mắc bệnh tiểu đường loại 2 tăng lên. Những người từ 50 tuổi trở lên thì tự động đạt mức điểm nguy cơ cao hơn do vấn đề tuổi tác. Bạn không thể dừng quá trình lão hóa, nhưng bạn có thể thực hiện các bước để giảm thiểu rủi ro thông qua thay đổi lối sống và \/ hoặc chế độ y tế.<\/p>"
           },
           {
              "id":"1.3",
              "value":"50-59 Years",
              "score":2,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/7-1-768x510.png",
              "label":"50-59 Years",
              "content":"<p>Khi bạn già đi, nguy cơ mắc bệnh tiểu đường loại 2 tăng lên. Những người từ 50 tuổi trở lên thì tự động đạt mức điểm nguy cơ cao hơn do vấn đề tuổi tác. Bạn không thể dừng quá trình lão hóa, nhưng bạn có thể thực hiện các bước để giảm thiểu rủi ro thông qua thay đổi lối sống và \/ hoặc chế độ y tế.<\/p>"
           },
           {
              "id":"1.4",
              "value":"60 years or older",
              "score":3,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/7-1-768x510.png",
              "label":"60+ Years",
              "content":"<p>Khi bạn già đi, nguy cơ mắc bệnh tiểu đường loại 2 tăng lên. Những người từ 50 tuổi trở lên thì tự động đạt mức điểm nguy cơ cao hơn do vấn đề tuổi tác. Bạn không thể dừng quá trình lão hóa, nhưng bạn có thể thực hiện các bước để giảm thiểu rủi ro thông qua thay đổi lối sống và \/ hoặc chế độ y tế.<\/p>"
           }
        ],
        "family":[
           {
              "id":"2.1",
              "value":"Yes",
              "score":1,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/9-1-768x760.png",
              "label":"Family History with Diabetes",
              "content":"<p>Có mối liên hệ giữa bệnh tiểu đường loại 2 và tiền sử gia đình, mặc dù nguy cơ cũng liên quan đến các yếu tố môi trường và lối sống mà các thành viên trong gia đình cùng gặp phải.<\/p>"
           },
           {
              "id":"2.2",
              "value":"No",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/9-1-768x760.png",
              "label":"No Family History with Diabetes",
              "content":"<p>Có mối liên hệ giữa bệnh tiểu đường loại 2 và tiền sử gia đình, mặc dù nguy cơ cũng liên quan đến các yếu tố môi trường và lối sống mà các thành viên trong gia đình cùng gặp phải.<\/p>"
           },
           {
              "id":"2.3",
              "value":"I\u2019m Not Sure",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/9-1-768x760.png",
              "label":"I\u2019m Not Sure",
              "content":"<p>Có mối liên hệ giữa bệnh tiểu đường loại 2 và tiền sử gia đình, mặc dù nguy cơ cũng liên quan đến các yếu tố môi trường và lối sống mà các thành viên trong gia đình cùng gặp phải.<\/p>"
           }
        ],
        "bloodPressure":[
           {
              "id":"3.1",
              "value":"Yes",
              "score":1,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/10-1-768x506.png",
              "label":"Blood Pressure History",
              "content":"<p>Huyết áp cao không chỉ làm tăng nguy cơ mắc bệnh tiểu đường loại 2 mà còn làm tăng nguy cơ cơn đau tim, đột quỵ, các vấn đề về mắt và bệnh thận.<\/p>"
           },
           {
              "id":"3.2",
              "value":"No",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/10-1-768x506.png",
              "label":"Blood Pressure History",
              "content":"<p>Huyết áp cao không chỉ làm tăng nguy cơ mắc bệnh tiểu đường loại 2 mà còn làm tăng nguy cơ cơn đau tim, đột quỵ, các vấn đề về mắt và bệnh thận.<\/p>"
           }
        ],
        "ethnicity":[
           {
              "id":"4.1",
              "value":"white",
              "score":0,
              "label":"White or Caucasian"
           },
           {
              "id":"4.2",
              "value":"latino",
              "score":0,
              "label":"Hispanic or Latino"
           },
           {
              "id":"4.3",
              "value":"african_american",
              "score":0,
              "label":"Black or African American"
           },
           {
              "id":"4.4",
              "value":"asian_american",
              "score":0,
              "label":"Asian American"
           },
           {
              "id":"4.5",
              "value":"american_indian",
              "score":0,
              "label":"American Indian or Alaska Native"
           },
           {
              "id":"4.6",
              "value":"islander",
              "score":0,
              "label":"Native Hawaiian or Other Pacific Islander"
           },
           {
              "id":"4.7",
              "value":"other",
              "score":0,
              "label":"Other"
           },
           {
              "id":"4.8",
              "value":"no_response",
              "score":0,
              "label":"Don\u2019t Want to Say"
           }
        ],
        "wellness":[
           {
              "id":"5.1",
              "value":"Yes",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/11-1-768x411.png",
              "label":"Physically Active",
              "content":"<p>Exercising regularly lowers your risk for prediabetes and type 2 diabetes.<\/p>"
           },
           {
              "id":"5.2",
              "value":"No",
              "score":1,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/11-1-768x411.png",
              "label":"Not Physically Active",
              "content":"<p>Exercising regularly lowers your risk for prediabetes and type 2 diabetes.<\/p>"
           }
        ],
        "gender":[
           {
              "id":"6.1",
              "value":"Man",
              "label":"Man",
              "score":1,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/8-1-768x456.png",
              "content":"<p>Men are more likely than women to have undiagnosed type 2 diabetes; one reason may be that they are less likely to see their doctor regularly.<\/p>"
           },
           {
              "id":"6.2",
              "value":"Woman",
              "label":"Woman",
              "score":0,
              "image":"https:\/\/doihaveprediabetes.org\/wp-content\/uploads\/2018\/09\/8-1-768x456.png",
              "content":"<p>Many women who have gestational diabetes go on to develop type 2 diabetes later in life.<\/p>"
           }
        ],
        "womanSub":[
           {
              "id":"7.1",
              "value":"Yes",
              "score":1,
              "label":"& Gestational Diabetes"
           },
           {
              "id":"7.2",
              "value":"No",
              "score":0,
              "label":"& No Gestational Diabetes"
           }
        ],
        "bmi": [{"height":"147cm","weight":["<54 kg","54-64 kg","64-86 kg",">86 kg"]},{"height":"150cm","weight":["<56 kg","56-67 kg","67-89 kg",">89 kg"]},{"height":"152cm","weight":["<58 kg","58-69 kg","69-92 kg",">92 kg"]},{"height":"155cm","weight":["<60 kg","60-71 kg","71-95 kg",">95 kg"]},{"height":"157cm","weight":["<62 kg","62-74 kg","74-98 kg",">98 kg"]},{"height":"160cm","weight":["<66 kg","66-78 kg","78-105 kg",">105 kg"]},{"height":"163cm","weight":["<66 kg","66-78 kg","78-105 kg",">105 kg"]},{"height":"165cm","weight":["<68 kg","68-81 kg","81-108 kg",">108 kg"]},{"height":"168cm","weight":["<70 kg","70-85 kg","84-112 kg",">112 kg"]},{"height":"170cm","weight":["<72 kg","72-86 kg","86-115 kg",">115 kg"]},{"height":"173cm","weight":["<74 kg","74-89 kg","89-118 kg",">118 kg"]},{"height":"175cm","weight":["<77 kg","77-92 kg","92-122 kg",">122 kg"]},{"height":"178cm","weight":["<79 kg","79-94 kg","94-126 kg",">126 kg"]},{"height":"180cm","weight":["<81 kg","81-97 kg","97-129 kg",">129 kg"]},{"height":"183cm","weight":["<83 kg","83-100 kg","100-133 kg",">133 kg"]},{"height":"185cm","weight":["<86 kg","86-103 kg","103-137 kg",">137 kg"]},{"height":"188cm","weight":["<88 kg","88-105 kg","105-141 kg",">141 kg"]},{"height":"191cm","weight":["<91 kg","91-108 kg","108-144 kg",">144 kg"]},{"height":"193cm","weight":["<93 kg","93-111 kg","111-148 kg",">148 kg"]}]
    },
    currentIndex: 0,
    user_answers: {womanSub:false},
    bmi: [{"score":0,"image":"https://doihaveprediabetes.org/wp-content/uploads/2018/09/13-1-768x882.png","label":"Chỉ số khối cơ thể (BMI) bình thường","content":"<p>BMI là chỉ số đo chiều cao so với cân nặng của bạn (kg/m2). Có chỉ số BMI cao hơn làm tăng nguy cơ mắc bệnh tiểu đường loại 2.</p>"},{"score":1,"image":"https://doihaveprediabetes.org/wp-content/uploads/2018/09/13-1-768x882.png","label":"Chỉ số khối cơ thể cao hơn mức bình thường","content":"<p>BMI là chỉ số đo chiều cao so với cân nặng của bạn (kg/m2). Có chỉ số BMI cao hơn làm tăng nguy cơ mắc bệnh tiểu đường loại 2.</p>"},{"score":2,"image":"https://doihaveprediabetes.org/wp-content/uploads/2018/09/13-1-768x882.png","label":"Chỉ số khối cơ thể cao","content":"<p>BMI là chỉ số đo chiều cao so với cân nặng của bạn (kg/m2). Có chỉ số BMI cao hơn làm tăng nguy cơ mắc bệnh tiểu đường loại 2.</p>"},{"score":3,"image":"https://doihaveprediabetes.org/wp-content/uploads/2018/09/13-1-768x882.png","label":"Chỉ số khối cơ thể rất caox","content":"<p>BMI là chỉ số đo chiều cao so với cân nặng của bạn (kg/m2). Có chỉ số BMI cao hơn làm tăng nguy cơ mắc bệnh tiểu đường loại 2.</p>"}],
    labels_ans: ["years","family","bloodPressure","ethnicity","wellness","gender","womanSub","bmi"],
    doned: false
}

const getters = {
    labels: (state)         =>  state.labels.map( label => { 
        return {
            slug: label, 
            label: state.questions[label].label
        }
    }), 
    currentIndex: (state)   =>  state.currentIndex,
    currentLabel: (state)   =>  state.labels[state.currentIndex],
    question: (state)       =>  {
        const label     =   state.labels[state.currentIndex];
        const question  =   state.questions[label];
        const variants    =   state.variants[label];

        return JSON.parse(JSON.stringify({...question, variants}))
    },
    subQuestion: (state)    =>  {
        let label = state.labels[state.currentIndex];

        if ( label !== 'gender' ) return false;
        
        label = 'womanSub';
        const question  =   state.questions[label];
        const variants    =   state.variants[label];

        return JSON.parse(JSON.stringify({...question, variants}))
    },
    user_answer: (state)      =>  state.user_answers[state.labels[state.currentIndex]],
    user_answer_sub: (state)  =>  state.user_answers['womanSub'],
    labels_ans:   (state)     =>  state.labels_ans ,
    user_answers:   (state)   =>  state.user_answers ,
    doned:   (state)   =>  state.doned ,
}

const actions = {
    answer( {state, commit}, {label, answer} ) {
        const theAnswer = {...state.variants[label][answer], answer};
        commit('setAnswer', {label, theAnswer} )
    },
    bmi( {state, commit}, {label, answer} ) {
         const result = state.bmi[answer.weight];
         commit('setAnswer', {label, theAnswer: {...result, answer}} )
    },
    previous( {state, commit} ) {
        if ( state.currentIndex > 0 ) {
            commit('previousQuestion');
        }
    },
    deleteWoman( {state, commit} ) {
       state.user_answers['womanSub'] = false
    },
    next( {state, commit} ) {
       if ( state.currentIndex < 6 ) commit('nextQuestion');
    }
}

const mutations = {
    setAnswer: (state, {label, theAnswer} ) => state.user_answers[label] = theAnswer,
    nextQuestion: (state, dispatch)   =>  state.currentIndex++,
    previousQuestion: (state)   => state.currentIndex--,
    set_doned: (state)  => state.doned = true,
    reset_counter: (state) => state.currentIndex = 0
}

export default {
    state,
    getters,
    mutations,
    actions
}