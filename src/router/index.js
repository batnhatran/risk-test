import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/answers',
    name: 'Reviews',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Reviews.vue')
  },
  {
    path: '/result',
    name: 'Result',
    component: () =>  import( '../views/Result.vue' )
  },
  {
    path: '/about',
    name: 'About',
    component: () =>  import( '../views/About.vue' )
  }
]

const router = new VueRouter({
  routes
})

export default router
